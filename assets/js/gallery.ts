import 'photoswipe'
import * as NaturalGallery from '@ecodev/natural-gallery-js'

declare global {
  interface Window { initGallery: any }
}

window.initGallery = async (container: HTMLElement) => {
  // Get gallery content
  let contentJSON = container.querySelector('#gallery-content')?.textContent;

  if (contentJSON) {
    let items = JSON.parse(contentJSON);
    if (!Modernizr.avif) {
      if (Modernizr.webp) {
        // Webp fallback.
        for (const image of items) {
          image.enlargedSrc = image.webpEnlargedSrc;
          image.thumbnailSrc = image.webpThumbSrc;
        }
      } else {
        // Jpeg fallback.
        for (const image of items) {
          image.enlargedSrc = image.jpegEnlargedSrc;
          image.thumbnailSrc = image.jpegThumbSrc;
        }
      }
    }

    // Gallery options
    let options = {
      backgroundSize: 'contain',
      gap: 9,
      lightbox: true,
      rowHeight: 400,
      photoSwipeOptions: { history: false, },
    };

    // Create gallery according to wanted format
    var gallery = new NaturalGallery.Natural(container, options); // or
    // var gallery = new NaturalGallery.Masonry(container, options, lightbox);

    // Add items to gallery
    gallery.setItems(items);

    // Init the gallery
    gallery.init();
  }
}