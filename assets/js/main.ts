import 'unpoly'
import {loadLink, loadScript} from './loadAsset'


// Async font loading
declare global {
  interface Window { WebFontConfig: any; }
}
window.WebFontConfig = {
   google: {
      families: ['Halant', 'Lato:300,400,700,300italic']
    }
};
(function(d) {
  var wf = d.createElement('script'), s = d.scripts[0];
  wf.src = 'https://ajax.googleapis.com/ajax/libs/webfont/1.6.26/webfont.js';
  wf.async = true;
  s.parentNode?.insertBefore(wf, s);
})(document);


// Mobile menu.
let mobileNav: HTMLElement
up.compiler('.mobile-menu-btn', async (btn: HTMLElement)=>{
  // Open mobile navigation bar when called.
  function openMobileMenu() {
    // Create mobile nav from footer nav on page and cache it for future opens.
    if (!mobileNav) {
      mobileNav = document.querySelector("footer section nav")?.cloneNode(true) as HTMLElement;
      if (mobileNav) {
        mobileNav.id = "mobile-nav";
        mobileNav.className = "text-gray-700 text-3xl";
        mobileNav.firstElementChild?.classList.add("flex", "flex-col", "h-72", "justify-between");
      }
    }
    // Open mobile nav.
    up.layer.open({
      content: mobileNav,
      layer: "new",
      mode: "drawer",
      size: "grow",
      location: false,
      animation: "move-from-right",
      position: "left",
    });
  }

  // Open mobile navigation bar when user taps mobile menu button.
  btn?.addEventListener('click', openMobileMenu);
})


function scrollToY(targetY: number, duration: number) {
  let startingY = window.pageYOffset;
  let diff = targetY - startingY;
  let start: number;

  // Bootstrap our animation - it will get called right before next frame shall be rendered.
  window.requestAnimationFrame(function step(timestamp) {
    if (!start) start = timestamp;
    // Elapsed milliseconds since start of scrolling.
    let time = timestamp - start;
    // Get percent of completion in range [0, 1].
    let percent = Math.min(time / duration, 1);

    window.scrollTo(0, startingY + diff * percent);

    // Proceed with animation as long as we wanted it to.
    if (time < duration) {
      window.requestAnimationFrame(step);
    }
  })
}

function scrollToSelector(query: string, duration = 400) {
  let target = document.querySelector(query);
  if (!target) return;  // Do nothing if no target found.
  // Scroll to vertical position of target.
  let y = window.pageYOffset + target.getBoundingClientRect().top;
  scrollToY(y, duration);
}

up.compiler('.hero-slider', async (el: HTMLElement)=>{
  let splideLoader = loadScript('/js/splide/splide.min.js');

  Modernizr.on('webp', async function(result: boolean) {
    if (!result) { // Fallback to jpeg for old devices

      async function fallbackImg(el: Element): Promise<void> {
        let webpPath = el.getAttribute("data-fallback-lazy");
        if (webpPath) {
          el.setAttribute("data-splide-lazy", webpPath);
        }
      }

      let routines: Promise<void>[] = [];
      let fallbackList = el.querySelectorAll('img[data-fallback-lazy]');

      for (let img of fallbackList) {
        routines.push(fallbackImg(img));
      }

      await Promise.all(routines);
    }
  })

  await splideLoader;
  new Splide( el, {
    pagination: false,
    lazyLoad: "sequential",
    type: "fade",
    rewind: true,
    autoplay: true,
    interval: 8000,
    speed: 800,
    pauseOnHover: false,
    arrows: false,
  }).mount();

  // Add home class to body of homepage.
  document.body.classList.add("home");

  // Get all internal links on homepage.
  let originalLinks = document.querySelectorAll('nav a[href^="/#"');
  let newLinks: Node[] = [];  // Register for modified instance of links.

  // Add smooth scrolling to internal links on homepage.
  for (let link of originalLinks) {
    let newLink = link.cloneNode(true);
    (newLink as HTMLAnchorElement).classList.add("focus:text-white")
    newLink.addEventListener("click", function(this: HTMLAnchorElement, evt: Event) {
      evt.stopPropagation();
      evt.preventDefault();

      let id = this.href.slice(this.href.indexOf('#'));
      scrollToSelector(id);
    })
    link.parentNode?.replaceChild(newLink, link);
    newLinks.push(newLink);
  }

  // Cleanup vestigial content before leaving homepage.
  let unbindListener = up.on('up:link:follow', () => {
    document.body.classList.remove("home");
    for (let i in newLinks) {
      let link = newLinks[i];
      link.parentNode?.replaceChild(originalLinks[i], link);
    }

    unbindListener();
  })

  let cont = el.querySelector("a.continue-btn")
  cont?.addEventListener("click", function(this: HTMLAnchorElement, evt: Event) {
    evt.stopPropagation();
    evt.preventDefault();

    let id = this.href.slice(this.href.indexOf('#'));
    scrollToSelector(id);
  });
});

up.compiler('.testimony-slider', async (el: HTMLElement)=>{
  await loadScript('/js/splide/splide.min.js');
  new Splide( el, {
    autoWidth: true,
    autoHeight: true,
    pagination: "slide",
    lazyLoad: "sequential",
    type: "fade",
    rewind: true,
    autoplay: true,
    interval: 8000,
    speed: 800,
    arrows: false,
    classes: {
      pagination: "flex items-center order-first w-full static -top-44 flex-row justify-center sm:-top-40 lg:justify-end",
      page: "splide__pagination__page bg-gray-200 flex w-8 h-1 rounded-none",
    }
  }).mount();
});

up.compiler('form.contact-form', async (form: HTMLFormElement) => {
  loadScript("https://www.google.com/recaptcha/api.js");
  for (let div of form.querySelectorAll("div.website, div.agree")) {
    div.classList.add("hidden");
  }
  // Remove input element that block form submission (blocks bots that try to instantly submit form).
  setTimeout(()=> { (<HTMLInputElement> form.querySelector("input[name=\"timeout\"]")).remove() } , 4000);
  await loadScript("/js/contactForm.js");
  initContactForm();
});

up.compiler('.gallery-room', async (container: HTMLElement) => {
  loadLink("/css/gallery/gallery.min.css")
  await loadScript("/js/gallery.js")
  initGallery(container)
});

up.compiler('#search', (_sb: HTMLDivElement) => {
  new PagefindUI({ element: "#search" });
});

up.compiler('.search-wrapper', (wrapper: HTMLDivElement) => {
  let checkbox = wrapper.querySelector(".toggle-state") as HTMLInputElement;
  let searchInput = wrapper.querySelector("#search .pagefind-ui__search-input") as HTMLInputElement;
  checkbox.addEventListener("change", () => {
    if (checkbox.checked) {
      searchInput.focus();
      searchInput.select();
    }
  })
  document.onkeydown = (evt) => {
    if (evt.key === 'F' && evt.ctrlKey) {
      checkbox.checked = !checkbox.checked;
      if (checkbox.checked) {
        searchInput.focus();
        searchInput.select();
      }
    }
  }
});