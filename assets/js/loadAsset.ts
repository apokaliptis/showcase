"use strict"

interface LoadLinkOptions {
  rel: string
  as?: string | undefined
}

interface LoadScriptOptions {
  defer?: boolean
}

class LoadResponse {
  element: HTMLElement
  options: LoadLinkOptions | LoadScriptOptions
  status: string
  url: string
  error?: string | Event
  #callback: Function

  constructor(
    url: string,
    el: HTMLLinkElement | HTMLScriptElement,
    callback: Function,
    options: LoadLinkOptions | LoadScriptOptions,
    status = "pending",
    error?: string | Event
    )
  {
    this.element = el
    this.options = options
    this.status = status
    this.url = url
    if (error) this.error = error
    this.#callback = callback
  }

  reload() {
    this.#callback(this.url, this.options)
  }
}

let loadedAssets: Map<string, Promise<LoadResponse>> = new Map();

function loadResponseHandler(
  url: string,
  el: HTMLLinkElement | HTMLScriptElement,
  callback: Function,
  options: LoadLinkOptions | LoadScriptOptions
  ): Promise<LoadResponse>
{
  let response = new LoadResponse(url, el, callback, options)
  let promise = new Promise<LoadResponse>((resolve, reject)=> {
      el.onload = ()=> {
        response.status = "success"
        resolve(response)
      }
      el.onerror = (err)=> {
        response.status = "failed"
        response.error = err
        reject(response)
      }
      document.head.appendChild(el)
    })
    loadedAssets.set(url, promise)
  return promise
}

function linkLoader(url: string, options: LoadLinkOptions): Promise<LoadResponse> {
  let el = document.createElement("link")
  el.rel = options.rel
  el.as = options.as ? options.as : ""
  el.href = url
  return loadResponseHandler(url, el, linkLoader, options)
}

function scriptLoader(url: string, options: LoadScriptOptions): Promise<LoadResponse> {
  let el = document.createElement("script")
  if (options.defer === undefined) {
    el.async = true
  } else {
    el.defer = options.defer
  }
  el.src = url
  return loadResponseHandler(url, el, scriptLoader, options)
}

function loadAsset(ref: string, options: LoadLinkOptions | LoadScriptOptions, callback: Function): Promise<LoadResponse> {
  let resolvedURL = new URL(ref, document.baseURI)
  let cache: Promise<LoadResponse> | undefined
  if (cache = loadedAssets.get(resolvedURL.href)) {
    return cache
  } else {
    return callback(resolvedURL.href, options)
  }
}

export async function loadLink(ref: string, options: LoadLinkOptions = { rel: "stylesheet" }): Promise<LoadResponse> {
  return loadAsset(ref, options, linkLoader)
}

export async function loadScript(ref: string, options: LoadScriptOptions = {}): Promise<LoadResponse> {
  return loadAsset(ref, options, scriptLoader)
}