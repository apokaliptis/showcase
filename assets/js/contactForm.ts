// Recaptcha callback
declare global {
  interface Window { gSubmitForm: any; initContactForm: any; }
}


function grayEmptySelect(select: HTMLSelectElement) {
  if (select.options[select.selectedIndex].value === "") {
    select.style.color = "#9ca3af"
  }
  else {
    select.style.color = ""
  }
}

function autoGrayEmptySelects(el = document.body as HTMLElement, query = "select") {
  el.querySelectorAll(query).forEach(async (select) => {
    grayEmptySelect(select as HTMLSelectElement)
    select.addEventListener("change", (evt: Event)=>{
      grayEmptySelect(evt.target as HTMLSelectElement)
    })
  })
}

function selectConditionals(form: HTMLElement = document.body, query: string = '.select-conditional') {
  form.querySelectorAll(query).forEach(async (container) => {
    // Remembers which form element is opened
    let lastOpened: string
    let cachedContent: {[key: string]: HTMLElement} = {}
    let condition = container.querySelector(
      "select.condition") as HTMLSelectElement

    if (condition) {

      // Fade out and remove elements
      async function fadeRemove(el: HTMLElement) {
        el.classList.add("fade-out")
        setTimeout(()=> {
          container.removeChild(el)
          el.classList.remove("fade-out")
        }, 1000)
      }

      // Reveal hidden form segment.
      function showFormBranch(branchName: string) {
        let content: HTMLElement

        // Check if we already loaded this element before.
        if (cachedContent.hasOwnProperty(branchName)) {
          content = cachedContent[branchName]
        } else {

          // Look for queried template.
          let tmpl = container.querySelector(".if-" + branchName) as HTMLTemplateElement

          // Make sure this selected element is a template.
          if (tmpl && 'content' in tmpl) {
            content = tmpl.content.firstElementChild?.cloneNode(true) as HTMLElement
            if (!content) {
              throw new Error("Content of template is empty.")
            }
          }

          // Otherwise reject promise.
          else {
            throw new Error("No template element found matching selector: " + branchName)
          }
        }

        // Insert into dom and cache the reference.
        autoGrayEmptySelects(content)
        cachedContent[branchName] = container.appendChild(content)
      }

      const cachedSelected = (
        <HTMLOptionElement> condition.children[condition.selectedIndex]
        ).dataset?.show

      if (cachedSelected) {
        showFormBranch(cachedSelected)
        lastOpened = cachedSelected
      }

      condition.addEventListener("change", async (evt: Event) => {
        let el = evt.target as HTMLSelectElement
        let branchName = (
          <HTMLOptionElement> el.children[el.selectedIndex]
          )?.dataset.show
        if (branchName !== lastOpened) {
          if (branchName) {
            showFormBranch(branchName)
          } else {
            branchName = ""
          }
          if (lastOpened) fadeRemove(cachedContent[lastOpened])
          lastOpened = branchName
        }
      })
    }
    else { console.log("Error: \"select.condition\" could not be found.") }
  })
}

window.gSubmitForm = async (token: any) => {
  let contactBox = document.getElementById("contact") as HTMLElement
  let contactForm = contactBox.querySelector("form") as HTMLFormElement
  let submitBtn = contactForm.querySelector("button.g-recaptcha") as HTMLButtonElement
  submitBtn.disabled = true

  async function validateSubmitForm() {
    // Validate form  and report errors to user.
    if (contactForm.reportValidity()) {
      // Track form submissions.
      try {
        gtag('event', 'form_submission')
      }
      catch(err) {
        console.log(err)
      }
      // Submit form and wait for response
      await up.submit(contactForm, { target: ".server-response", navigation: false})
      // Scroll to server response
      let serverResponseBox = contactBox.querySelector(".server-response") as HTMLElement
      serverResponseBox.scrollIntoView({ block: 'end',  behavior: 'smooth' })
      return true
    } else {
      submitBtn.disabled = false
      return false
    }
  }

  // Validate and submit form now.
  if (!await validateSubmitForm()) {
    // If validation fails, listen for retries.
    let newBtn = document.createElement("button")
    newBtn.className = submitBtn.className
    newBtn.textContent = submitBtn.textContent
    contactForm.replaceChild(newBtn, submitBtn)
    contactForm.addEventListener("submit", (evt: Event)=> {
      evt.preventDefault()
      validateSubmitForm()
    })
  }
}

window.initContactForm = async ()=> {
  autoGrayEmptySelects()
  selectConditionals()
}