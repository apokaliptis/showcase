module.exports = {
  plugins: [
    require('autoprefixer'),
    require('tailwindcss')(__dirname + '/tailwind.config.js'),
    /* ...process.env.HUGO_ENVIRONMENT === 'production'
      ? []
      : [] */
  ]
}